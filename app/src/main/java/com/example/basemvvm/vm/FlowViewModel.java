package com.example.basemvvm.vm;

import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.example.basemvvm.model.NavigationEvent;

public class FlowViewModel extends ViewModel {
    private MutableLiveData<NavigationEvent> flowLiveData;

    public FlowViewModel() {
        flowLiveData = new MutableLiveData<>();
    }

    public void idle(){
        flowLiveData.postValue(new NavigationEvent(NavigationEvent.NavAction.next, null));
    }

    public void next(Object payload){
        flowLiveData.postValue(new NavigationEvent(NavigationEvent.NavAction.next, payload));
    }

    public void prev(Object payload){
        flowLiveData.postValue(new NavigationEvent(NavigationEvent.NavAction.prev, payload));
    }

    public void close(Object payload){
        flowLiveData.postValue(new NavigationEvent(NavigationEvent.NavAction.close, payload));
    }

    public void refresh(Object payload){
        flowLiveData.postValue(new NavigationEvent(NavigationEvent.NavAction.refresh, payload));
    }

    public MutableLiveData<NavigationEvent> exposeLiveData(){
        return flowLiveData;
    }
}
