package com.example.basemvvm.util;

import android.util.Log;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.example.basemvvm.R;

import org.jetbrains.annotations.NotNull;

public class FragmentUtils { //id for all fragment containers should be flContainer

    public static void addFragment(@NotNull FragmentManager fm, Fragment f) {
        Log.e("tag", "addFragment: " + fm.getBackStackEntryCount() );
        fm.beginTransaction().add(R.id.flContainer, f, f.getClass().getSimpleName()).addToBackStack(f.getClass().getSimpleName()).commit();
        Log.e("tag", "addFragment: " + fm.getBackStackEntryCount() );
    }

    public static void replaceFragment(@NotNull FragmentManager fm, Fragment f) {
        for (int i = 0; i < fm.getFragments().size(); i++) {
            fm.popBackStackImmediate();
        }

        fm.beginTransaction()
                .replace(R.id.flContainer, f, f.getClass().getSimpleName())
                .addToBackStack(f.getClass().getSimpleName())
                .commitAllowingStateLoss();
    }

    public static void popFragment(@NotNull FragmentManager fm) {
        fm.popBackStack();
    }

    public static void popSpecificFragment(@NotNull FragmentManager fm, String tag) {
        Log.e("tag", "pop: " + fm.getBackStackEntryCount() );
        Fragment fragment = fm.findFragmentByTag(tag);
        if (fragment != null)
            fm.beginTransaction().remove(fragment).commit();
        Log.e("tag", "pop: " + fm.getBackStackEntryCount() );
    }

    public static boolean checkFragmentExistence(@NotNull FragmentManager fm, String tag) {
        for (int i = 0; i < fm.getFragments().size(); i++) {
            if (fm.getFragments().get(i).getClass().getSimpleName().equals(tag))
                return true;
        }
        return false;
    }

    public static void popUntilFind(@NotNull FragmentManager fm, Fragment f) {
        for (int i = fm.getFragments().size() - 1; i >= 0; i--) {
            if (!fm.getFragments().get(i).getClass().getSimpleName().equals(f.getClass().getSimpleName()))
                popSpecificFragment(fm, f.getClass().getSimpleName());
        }
    }

    public static void reAttach(){

    }

}
