package com.example.basemvvm.model;

public class NavigationEvent {
    public enum NavAction{
        next, prev, close, refresh, idle
    }

    private NavAction navAction;
    private Object payload; //used to deliver data

    public NavigationEvent(NavAction navAction, Object payload) {
        this.navAction = navAction;
        this.payload = payload;
    }

    public NavigationEvent(NavAction navAction) {
        this.navAction = navAction;
        this.payload = null;
    }

    public NavAction getNavAction() {
        return navAction;
    }
}
