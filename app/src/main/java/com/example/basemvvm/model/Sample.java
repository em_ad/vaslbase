package com.example.basemvvm.model;

import androidx.annotation.Nullable;
import androidx.room.Entity;
import androidx.room.PrimaryKey;

import com.google.gson.annotations.SerializedName;

import java.io.Serializable;

@Entity
public class Sample implements Serializable {

    @PrimaryKey(autoGenerate = true)
    private int id;

    @SerializedName("Server Name for Attribute i")
    private int i;
    @SerializedName("Server Name for Attribute s")
    private String s;

    public Sample(int i, String s) {
        this.id = 0;
        this.i = i;
        this.s = s;
    }

    public int getI() {
        return i;
    }

    public String getS() {
        return s;
    }

    public void setI(int i) {
        this.i = i;
    }

    public void setS(String s) {
        this.s = s;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public boolean equals(@Nullable Object obj) {
        if(obj == null || obj.getClass() != this.getClass()) return false;
        Sample sample = (Sample) obj;
        return i == sample.i && s.equals(sample.s);
    }
}
