package com.example.basemvvm.base;

import android.util.Log;

import androidx.fragment.app.Fragment;

import com.example.basemvvm.util.FragmentUtils;

public class BaseFragment extends Fragment {

    public void closeFragment(String tag){
        Log.e("tag", "closeFragment: " + getTag() );
        if(selfCheck() && getActivity()!= null)
            FragmentUtils.popSpecificFragment(getActivity().getSupportFragmentManager(), tag);
    }

    public void refreshFragment(){
        if(!selfCheck() || getActivity() == null)
            return;
        try {
            getActivity().getSupportFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
            Log.e(this.getTag(), "refreshFragment: SUCCESS" );
        }
        catch (Exception e){
            Log.e(this.getTag(), "refreshFragment: FAILED (no such fragment in activity)" );
        }
        try {
            getChildFragmentManager()
                    .beginTransaction()
                    .detach(this)
                    .attach(this)
                    .commit();
            Log.e(this.getTag(), "refreshFragment: SUCCESS" );
        }
        catch (Exception e){
            Log.e(this.getTag(), "refreshFragment: FAILED (no such fragment in fragment)" );
        }
    }

    public boolean selfCheck(){
        return isAdded() && getContext()!=null;
    }

}
