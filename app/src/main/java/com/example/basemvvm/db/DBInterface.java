package com.example.basemvvm.db;

import androidx.room.Database;
import androidx.room.RoomDatabase;

import com.example.basemvvm.model.Sample;
import com.example.basemvvm.util.Constants;

@Database(entities = {Sample.class}, version = Constants.DB_VERSION, exportSchema = false)
public abstract class DBInterface extends RoomDatabase {
    public abstract DataAccessObject accessObject();
}
