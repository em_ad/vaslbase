package com.example.basemvvm.db;

import androidx.room.Room;

import com.example.basemvvm.MyApplication;
import com.example.basemvvm.util.Constants;

public class DBAccess {

    private static DBAccess dBaccess = null;

    private DBInterface db;

    private DBAccess() {
        db = Room.databaseBuilder(MyApplication.getContext(), DBInterface.class, Constants.ROOM_NAME).allowMainThreadQueries().build();
        dBaccess = this;
    }

    public static DBAccess getInstance() {
        if (dBaccess == null)
            dBaccess = new DBAccess();
        return dBaccess;
    }

    public DBInterface getDb() {
        return db;
    }
}
