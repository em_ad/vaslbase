package com.example.basemvvm;

import android.app.Application;

public class MyApplication extends Application {
    private static Application app;

    public static Application getContext() {return app;}

    @Override
    public void onCreate() {
        super.onCreate();
        app = this;
    }
}
