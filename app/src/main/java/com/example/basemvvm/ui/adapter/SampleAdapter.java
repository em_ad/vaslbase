package com.example.basemvvm.ui.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.paging.PagedListAdapter;
import androidx.recyclerview.widget.DiffUtil;
import androidx.recyclerview.widget.RecyclerView;

import com.example.basemvvm.R;
import com.example.basemvvm.model.Sample;

public class SampleAdapter extends PagedListAdapter<Sample, SampleAdapter.ViewHolder> {

    private Context context;

    protected SampleAdapter() {
        super(new DiffUtil.ItemCallback<Sample>() {
            @Override
            public boolean areItemsTheSame(@NonNull Sample oldItem, @NonNull Sample newItem) {
                return false; //todo -> write the condition in which the object is the same object and different content. e.g. check object id
            }

            @Override
            public boolean areContentsTheSame(@NonNull Sample oldItem, @NonNull Sample newItem) {
                return oldItem.equals(newItem); //objects are the same
            }
        });
    }

    @NonNull
    @Override
    public ViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new ViewHolder(LayoutInflater.from(context)
                        .inflate(R.layout.viewholder_design, parent, false));
    }

    @Override
    public void onBindViewHolder(@NonNull ViewHolder holder, int position) {

    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public ViewHolder(@NonNull View itemView) {
            super(itemView);
        }
    }
}
