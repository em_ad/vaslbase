package com.example.basemvvm.ui;

import android.os.Bundle;
import android.os.Handler;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.annotation.Nullable;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.example.basemvvm.R;
import com.example.basemvvm.base.BaseFragment;
import com.example.basemvvm.model.NavigationEvent;
import com.example.basemvvm.vm.FlowViewModel;

public class MainFragment extends BaseFragment {

    public static MainFragment newInstance() {
        return new MainFragment();
    }

    private FlowViewModel flowViewModel;

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        return inflater.inflate(R.layout.fragment_main, container, false);
    }

    @Override
    public void onViewCreated(@NonNull View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        flowViewModel = new ViewModelProvider(getActivity() != null ? getActivity() : this).get(FlowViewModel.class);
        flowViewModel.exposeLiveData().observe(getViewLifecycleOwner(), new Observer<NavigationEvent>() {
            @Override
            public void onChanged(NavigationEvent navigationEvent) {
                switch (navigationEvent.getNavAction()) {
                    case idle:
                        break;
                    case close:
                        closeFragment("MainFragment");
                        break;
                    case next:
                        break;
                    case prev:
                        break;
                    case refresh:
                        refreshFragment();
                        break;
                }
            }
        });
    }
}
