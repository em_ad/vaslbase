package com.example.basemvvm.ui;

import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.lifecycle.ViewModelProvider;

import com.bumptech.glide.Glide;
import com.example.basemvvm.MyApplication;
import com.example.basemvvm.R;
import com.example.basemvvm.base.BaseActivity;
import com.example.basemvvm.util.FragmentUtils;
import com.example.basemvvm.vm.FlowViewModel;

import static com.example.basemvvm.MyApplication.getContext;

public class MainActivity extends BaseActivity implements View.OnClickListener {

    private TextView tvOpen;
    private TextView tvClose;
    private FlowViewModel flowViewModel;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        flowViewModel = new ViewModelProvider(this).get(FlowViewModel.class);

        Glide.with(this).load(getString(R.string.test_image_url)).into((ImageView) findViewById(R.id.ivTop));

        findViews();
        setListeners();
    }

    private void setListeners() {
        tvClose.setOnClickListener(this);
        tvOpen.setOnClickListener(this);
    }

    @Override
    public void findViews() {
        tvClose = findViewById(R.id.tvClose);
        tvOpen = findViewById(R.id.tvOpen);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.tvClose:
                if (!FragmentUtils.checkFragmentExistence(getSupportFragmentManager(), "MainFragment")) {
                    Toast.makeText(this, "Fragment is Already Closed", Toast.LENGTH_SHORT).show();
                } else {
                    flowViewModel.close(null);
                    Toast.makeText(getContext(), "Fragment Closed", Toast.LENGTH_SHORT).show();
                }
                break;
            case R.id.tvOpen:
                flowViewModel.idle();
                if (FragmentUtils.checkFragmentExistence(getSupportFragmentManager(), "MainFragment")) {
                    Toast.makeText(this, "Fragment is Already Opened", Toast.LENGTH_SHORT).show();
                } else {
                    FragmentUtils.addFragment(getSupportFragmentManager(), MainFragment.newInstance());
                    Toast.makeText(getContext(), "Fragment Opened", Toast.LENGTH_SHORT).show();
                }
                break;
        }
    }
}
